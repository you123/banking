package ru.aston.bank.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import ru.aston.bank.dto.*;
import ru.aston.bank.entity.Account;
import ru.aston.bank.entity.Transaction;
import ru.aston.bank.entity.TransactionType;
import ru.aston.bank.exception.*;
import ru.aston.bank.repository.AccountRepository;
import ru.aston.bank.repository.TransactionRepository;
import ru.aston.bank.validate.AccountValidator;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static ru.aston.bank.entity.TransactionType.TRANSFER;
import static ru.aston.bank.service.AccountUtil.getAccountNumber;

@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {

    @Mock
    private AccountRepository repository;
    @Mock
    private AccountValidator validator;

    @Mock
    TransactionRepository transactionRepository;
    @Mock
    private ModelMapper mapper;
    @InjectMocks
    private AccountServiceImpl service;

    private AccountDtoCreation dtoCreation;
    private AccountDtoOperation dtoOperation;

    @BeforeEach
    public void setup() {
        dtoCreation = AccountDtoCreation.builder()
                .name("sfreghhttjtj")
                .pin("3223")
                .build();
        dtoOperation = AccountDtoOperation.builder()
                .name("grgrgrgnhrhrhr")
                .pin("7898")
                .amount(BigDecimal.valueOf(30))
                .build();
    }

    @Test
    public void createAccountSuccess() {
        Account expectedAccount = Account.builder()
                .id(UUID.randomUUID())
                .name(dtoCreation.getName())
                .pin(dtoCreation.getPin())
                .number(getAccountNumber())
                .balance(BigDecimal.ZERO)
                .build();

        doNothing().when(validator).validate(dtoCreation);
        when(mapper.map(dtoCreation, Account.class)).thenReturn(expectedAccount);
        when(repository.save(expectedAccount)).thenReturn(expectedAccount);

        Account actualAccount = service.create(dtoCreation);

        assertThat(actualAccount).isEqualTo(expectedAccount);

        verify(validator).validate(dtoCreation);
        verify(mapper).map(dtoCreation, Account.class);
        verify(repository).save(expectedAccount);
    }


    @Test
    public void createAccountFailThrowAccountNotCreatedException() {
        doThrow(AccountNotCreatedException.class).when(validator).validate(dtoCreation);

        assertThrows(AccountNotCreatedException.class, (() ->
                validator.validate(dtoCreation)));

        verify(validator).validate(dtoCreation);
        verify(mapper, never()).map(dtoCreation, Account.class);
        verify(repository, never()).save(mapper.map(dtoCreation, Account.class));
    }

    @Test
    public void depositAccountSuccess() {
        Account expectedAccount = Account.builder()
                .id(UUID.randomUUID())
                .name(dtoOperation.getName())
                .pin(dtoOperation.getPin())
                .number(getAccountNumber())
                .balance(BigDecimal.valueOf(50))
                .build();

        Transaction transaction = new Transaction();
        transaction.setAmount(dtoOperation.getAmount());
        transaction.setTransactionType(TransactionType.DEPOSIT);
        transaction.setToWhom(expectedAccount);
        transaction.setFromWhom(expectedAccount);

        when(validator.validate(dtoOperation)).thenReturn(expectedAccount);

        Account actualAccount = service.deposit(dtoOperation);

        assertThat(actualAccount).isEqualTo(transaction.getFromWhom());

        verify(validator, times(2)).validate(dtoOperation);
    }

    @Test
    public void depositAccountFailThrowAccountException() {
        doThrow(AccountException.class).when(validator).validate(dtoOperation);

        assertThrows(AccountException.class, (() -> validator.validate(dtoOperation)));

        verify(validator).validate(dtoOperation);
        verify(repository, never()).save(mapper.map(dtoOperation, Account.class));
    }

    @Test
    public void depositAccountFailThrowAccountAuthorizationException() {
        doThrow(AccountAuthorizationException.class).when(validator).validate(dtoOperation);

        assertThrows(AccountAuthorizationException.class, (() -> validator.validate(dtoOperation)));

        verify(validator).validate(dtoOperation);
        verify(repository, never()).save(mapper.map(dtoOperation, Account.class));
    }

    @Test
    public void withdrawAccountSuccess() {
        Account expectedAccount = Account.builder()
                .id(UUID.randomUUID())
                .name(dtoOperation.getName())
                .pin(dtoOperation.getPin())
                .number(getAccountNumber())
                .balance(BigDecimal.valueOf(50))
                .build();

        Transaction transaction = new Transaction();
        transaction.setAmount(dtoOperation.getAmount());
        transaction.setTransactionType(TransactionType.WITHDRAW);
        transaction.setToWhom(expectedAccount);
        transaction.setFromWhom(expectedAccount);

        when(validator.validate(dtoOperation)).thenReturn(expectedAccount);

        Account actualAccount = service.withdraw(dtoOperation);

        assertThat(actualAccount).isEqualTo(transaction.getFromWhom());

        verify(validator).validate(dtoOperation);
    }

    @Test
    public void withdrawAccountFailThrowAccountException() {
        doThrow(AccountException.class).when(validator).validate(dtoOperation);

        assertThrows(AccountException.class, (() -> validator.validate(dtoOperation)));

        verify(validator).validate(dtoOperation);
        verify(repository, never()).save(mapper.map(dtoOperation, Account.class));
    }

    @Test
    public void withdrawAccountFailThrowAccountAuthorizationException() {
        doThrow(AccountAuthorizationException.class).when(validator).validate(dtoOperation);

        assertThrows(AccountAuthorizationException.class, (() -> validator.validate(dtoOperation)));

        verify(validator).validate(dtoOperation);
        verify(repository, never()).save(mapper.map(dtoOperation, Account.class));
    }

    @Test
    public void withdrawAccountFailThrowAccountOperationException() {
        Account account = Account.builder()
                .id(UUID.randomUUID())
                .name(dtoOperation.getName())
                .pin(dtoOperation.getPin())
                .number(getAccountNumber())
                .balance(BigDecimal.valueOf(20))
                .build();

        when(validator.validate(dtoOperation)).thenReturn(account);

        assertThrows(AccountOperationException.class, (() -> service.withdraw(dtoOperation)));

        verify(validator).validate(dtoOperation);
    }

    @Test
    public void transferAccountsSuccess() {
        AccountDtoTransfer dtoTransfer = AccountDtoTransfer.builder()
                .name("ExampleAccountName")
                .pin("1234")
                .nameToTransfer("ExampleAccountNameToTransfer")
                .amount(BigDecimal.valueOf(30))
                .build();
        dtoOperation.setName(dtoTransfer.getName());
        dtoOperation.setPin(dtoTransfer.getPin());
        dtoOperation.setAmount(dtoTransfer.getAmount());
        Account accountToWithdraw = Account.builder()
                .id(UUID.randomUUID())
                .name(dtoTransfer.getName())
                .pin(dtoTransfer.getPin())
                .number(getAccountNumber())
                .balance(BigDecimal.valueOf(50))
                .build();
        Account accountToDeposit = Account.builder()
                .id(UUID.randomUUID())
                .name(dtoTransfer.getNameToTransfer())
                .pin(dtoTransfer.getPin())
                .number(getAccountNumber())
                .balance(BigDecimal.valueOf(0))
                .build();

        Transaction transaction = new Transaction();
        transaction.setAmount(dtoOperation.getAmount());
        transaction.setTransactionType(TRANSFER);
        transaction.setToWhom(accountToDeposit);
        transaction.setFromWhom(accountToWithdraw);


        when(mapper.map(dtoTransfer, AccountDtoOperation.class)).thenReturn(dtoOperation);
        when(repository.findByName(dtoTransfer.getName())).thenReturn(Optional.of(accountToDeposit));
        when(repository.findByName(dtoTransfer.getNameToTransfer())).thenReturn(Optional.of(accountToDeposit));

        service.transfer(dtoTransfer);

        assertEquals(new BigDecimal("50"), accountToWithdraw.getBalance());
        assertEquals(new BigDecimal("0"), accountToDeposit.getBalance());


        verify(mapper).map(dtoTransfer, AccountDtoOperation.class);
        verify(repository).findByName(dtoTransfer.getName());
        verify(repository).findByName(dtoTransfer.getNameToTransfer());
        verify(transactionRepository).save(any(Transaction.class));
    }

    @Test
    public void transferAccountsFailThrowAccountNotFoundException() {
        AccountDtoTransfer dtoTransfer = AccountDtoTransfer.builder()
                .name("ExampleAccountName")
                .pin("1234")
                .nameToTransfer("ExampleAccountNameToTransfer")
                .amount(BigDecimal.valueOf(30))
                .build();
        dtoOperation.setName(dtoTransfer.getName());
        dtoOperation.setPin(dtoTransfer.getPin());
        dtoOperation.setAmount(dtoTransfer.getAmount());

        when(mapper.map(dtoTransfer, AccountDtoOperation.class)).thenReturn(dtoOperation);
        when(repository.findByName(dtoTransfer.getName())).thenReturn(Optional.empty());

        assertThrows(AccountNotFoundException.class, (() -> service.transfer(dtoTransfer)));

        verify(mapper).map(dtoTransfer, AccountDtoOperation.class);
        verify(repository).findByName(dtoTransfer.getName());
        verify(repository, never()).findByName(dtoTransfer.getNameToTransfer());
        verify(repository, never()).save(mapper.map(dtoOperation, Account.class));
    }

    @Test
    public void getAccountsSuccess() {
        Account account1 = Account.builder()
                .id(UUID.randomUUID())
                .name("ExampleName1")
                .pin("1234")
                .number(getAccountNumber())
                .balance(BigDecimal.valueOf(10))
                .build();
        Account account2 = Account.builder()
                .id(UUID.randomUUID())
                .name("ExampleName2")
                .pin("5678")
                .number(getAccountNumber())
                .balance(BigDecimal.valueOf(0))
                .build();
        AccountDtoResponse dtoResponse1 = AccountDtoResponse.builder()
                .name(account1.getName())
                .balance(account1.getBalance())
                .build();
        AccountDtoResponse dtoResponse2 = AccountDtoResponse.builder()
                .name(account2.getName())
                .balance(account2.getBalance())
                .build();
        List<Account> accounts = List.of(account1, account2);
        when(repository.findAll()).thenReturn(accounts);
        when(mapper.map(account1, AccountDtoResponse.class)).thenReturn(dtoResponse1);
        when(mapper.map(account2, AccountDtoResponse.class)).thenReturn(dtoResponse2);

        List<AccountDtoResponse> expected = List.of(dtoResponse1, dtoResponse2);
        List<AccountDtoResponse> actual = service.getAccounts();

        assertThat(actual).isEqualTo(expected);

        verify(repository).findAll();
        verify(mapper).map(account1, AccountDtoResponse.class);
        verify(mapper).map(account2, AccountDtoResponse.class);
    }

    @Test
    public void getTransactionsSuccess() {
        UUID uuid = UUID.randomUUID();
        Account account1 = Account.builder()
                .id(uuid)
                .name("ExampleName1")
                .pin("1234")
                .number(getAccountNumber())
                .balance(BigDecimal.valueOf(10))
                .build();
        Account account2 = Account.builder()
                .id(UUID.randomUUID())
                .name("ExampleName2")
                .pin("5678")
                .number(getAccountNumber())
                .balance(BigDecimal.valueOf(0))
                .build();

        Transaction transaction = Transaction.builder()
                .id(UUID.randomUUID())
                .amount(BigDecimal.valueOf(0))
                .transactionType(TRANSFER)
                .transactionDateTime(LocalDateTime.now())
                .fromWhom(account1)
                .toWhom(account2)
                .build();

        AccountDtoResponse dtoResponse1 = AccountDtoResponse.builder()
                .name(account1.getName())
                .balance(account1.getBalance())
                .build();
        AccountDtoResponse dtoResponse2 = AccountDtoResponse.builder()
                .name(account2.getName())
                .balance(account2.getBalance())
                .build();
        TransactionDtoResponse transactionDtoResponse = TransactionDtoResponse.builder()
                .id(transaction.getId())
                .amount(transaction.getAmount())
                .transactionType(transaction.getTransactionType())
                .fromWhom(dtoResponse1)
                .toWhom(dtoResponse2)
                .build();
        List<Transaction> transactionList = List.of(transaction);

        when(transactionRepository.findAllByFromWhomIdOrToWhomId(uuid, uuid)).thenReturn(transactionList);
        when(mapper.map(transaction, TransactionDtoResponse.class)).thenReturn(transactionDtoResponse);

        List<TransactionDtoResponse> expected = List.of(transactionDtoResponse);

        List<TransactionDtoResponse> actual = service.getTransactions(uuid);

        assertThat(actual).isEqualTo(expected);

        verify(transactionRepository).findAllByFromWhomIdOrToWhomId(uuid, uuid);
        verify(mapper).map(transaction, TransactionDtoResponse.class);
    }
}