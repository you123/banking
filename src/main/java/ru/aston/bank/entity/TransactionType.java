package ru.aston.bank.entity;

public enum TransactionType {
    DEPOSIT,
    WITHDRAW,
    TRANSFER
}
