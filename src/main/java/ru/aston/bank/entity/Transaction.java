package ru.aston.bank.entity;

import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "transactions")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class Transaction {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Enumerated(EnumType.STRING)
    @Column(name = "transactionType")
    private TransactionType transactionType;

    private BigDecimal amount;

    private LocalDateTime transactionDateTime = LocalDateTime.now();

    @ManyToOne
    @JoinColumn(name="from_whom_accounts_id", referencedColumnName = "id")
    private Account fromWhom;

    @ManyToOne
    @JoinColumn(name="to_whom_accounts_id", referencedColumnName = "id")
    private Account toWhom;
}
