package ru.aston.bank.entity;

import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "accounts")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class Account {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(name = "name")
    private String name;

    @Column(name = "pin")
    private String pin;

    @Column(name = "number")
    private String number;

    @Column(name = "balance")
    private BigDecimal balance = BigDecimal.ZERO;


    @OneToMany(mappedBy = "fromWhom", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Transaction> fromWhomList;

    @OneToMany(mappedBy = "toWhom", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Transaction> toWhomList;
}
