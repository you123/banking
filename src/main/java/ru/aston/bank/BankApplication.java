package ru.aston.bank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Set;

import static java.lang.String.join;

@SpringBootApplication
public class BankApplication {

    public static void main(String[] args) {
         SpringApplication.run(BankApplication.class, args);

        }
}
