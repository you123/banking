package ru.aston.bank.validate;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import ru.aston.bank.dto.AccountDtoCreation;
import ru.aston.bank.dto.AccountDtoOperation;
import ru.aston.bank.entity.Account;
import ru.aston.bank.exception.AccountAuthorizationException;
import ru.aston.bank.exception.AccountException;
import ru.aston.bank.exception.AccountNotCreatedException;
import ru.aston.bank.repository.AccountRepository;

import java.util.Optional;

@Component
@AllArgsConstructor
public class AccountValidator {
    private final AccountRepository accountRepository;

    public void validate(AccountDtoCreation accountDto) {
        if (accountRepository.existsByName(accountDto.getName())) {
            throw new AccountNotCreatedException("Счет с таким именем уже существует");
        }
    }

    public Account validate(AccountDtoOperation accountDto) {
        Optional<Account> accountOp = accountRepository.findByName(accountDto.getName());

        Account account = accountOp.orElseThrow(()->new AccountException("Счета с таким именем не существует"));

        if (!account.getPin().equals(accountDto.getPin())) {
            throw new AccountAuthorizationException("Неверный ПИН-код");
        }
        return account;
    }
}
