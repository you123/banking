package ru.aston.bank.controller;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.aston.bank.dto.*;
import ru.aston.bank.service.AccountService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("api/accounts")
@AllArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @PostMapping
    public ResponseEntity<String> create(@RequestBody @Valid AccountDtoCreation accountDto) {
        accountService.create(accountDto);
        return new ResponseEntity<>("Счет создан", HttpStatus.CREATED);
    }


    @PatchMapping("/deposit")
    public ResponseEntity<HttpStatus> deposit(@RequestBody @Valid AccountDtoOperation accountDto) {
        accountService.deposit(accountDto);

        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PatchMapping("/withdraw")
    public ResponseEntity<HttpStatus> withdraw(@RequestBody @Valid AccountDtoOperation accountDto) {
        accountService.withdraw(accountDto);

        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PatchMapping("/transfer")
    public ResponseEntity<HttpStatus> transfer(@RequestBody @Valid AccountDtoTransfer accountDto) {
        accountService.transfer(accountDto);

        return ResponseEntity.ok(HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<AccountDtoResponse>> getAccounts() {

        return new ResponseEntity<>(accountService.getAccounts(), HttpStatus.OK);
    }

    @GetMapping("/transactions/{id}")
    public ResponseEntity<List<TransactionDtoResponse>> getTransactions(@PathVariable("id") UUID accountId) {

        return new ResponseEntity<>(accountService.getTransactions(accountId), HttpStatus.OK);
    }
}
