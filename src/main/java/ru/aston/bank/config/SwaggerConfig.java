package ru.aston.bank.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI()
                .info(new Info().title("API Bank")
                        .description("REST API, служба поддержки и модель данных для создания банковских счетов\n" +
                                "и перевода денег между ними.")
                        .version("1.0"));
    }
}