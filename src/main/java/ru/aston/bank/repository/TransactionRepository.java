package ru.aston.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.aston.bank.entity.Transaction;

import java.util.List;
import java.util.UUID;

public interface TransactionRepository extends JpaRepository<Transaction, UUID> {
    List<Transaction> findAllByFromWhomIdOrToWhomId(UUID fromWhom, UUID toWhom);
}
