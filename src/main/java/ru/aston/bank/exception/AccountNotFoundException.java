package ru.aston.bank.exception;

public class AccountNotFoundException extends AccountException {
    public AccountNotFoundException(String msg) {
        super(msg);
    }
}
