package ru.aston.bank.exception;

public class AccountException extends RuntimeException {
    public AccountException(String msg) {
        super(msg);
    }
}
