package ru.aston.bank.exception;

public class AccountOperationException extends AccountException {
    public AccountOperationException(String msg) {
        super(msg);
    }
}
