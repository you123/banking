package ru.aston.bank.exception;

public class AccountNotCreatedException extends AccountException {
    public AccountNotCreatedException(String msg) {
        super(msg);
    }
}
