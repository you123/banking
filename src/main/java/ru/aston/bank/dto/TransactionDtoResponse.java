package ru.aston.bank.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import ru.aston.bank.entity.TransactionType;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;
@Getter
@Setter
@Builder
public class TransactionDtoResponse {
    private UUID id;
    private TransactionType transactionType;
    private BigDecimal amount;
    private LocalDateTime transactionDateTime;
    private AccountDtoResponse fromWhom;
    private AccountDtoResponse toWhom;
}
