package ru.aston.bank.service;

import ru.aston.bank.dto.*;
import ru.aston.bank.entity.Account;

import java.util.List;
import java.util.UUID;

public interface AccountService {

    Account create(AccountDtoCreation dtoCreation);

    Account deposit(AccountDtoOperation dtoOperation);

    Account withdraw(AccountDtoOperation dtoOperation);

    void transfer(AccountDtoTransfer dtoTransfer);

    List<AccountDtoResponse> getAccounts();

    List<TransactionDtoResponse> getTransactions(UUID uuid);
}
