package ru.aston.bank.service;

import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import ru.aston.bank.dto.*;
import ru.aston.bank.entity.Account;
import ru.aston.bank.entity.Transaction;
import ru.aston.bank.entity.TransactionType;
import ru.aston.bank.exception.AccountNotFoundException;
import ru.aston.bank.exception.AccountOperationException;
import ru.aston.bank.repository.AccountRepository;
import ru.aston.bank.repository.TransactionRepository;
import ru.aston.bank.validate.AccountValidator;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static ru.aston.bank.entity.TransactionType.*;

@Service
@AllArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final TransactionRepository transactionRepository;
    private final ModelMapper mapper;
    private final AccountValidator validator;

    @Override
    @Transactional
    public Account create(AccountDtoCreation dtoCreation) {
        validator.validate(dtoCreation);
        Account account = mapper.map(dtoCreation, Account.class);
        account.setNumber(AccountUtil.getAccountNumber());
        return accountRepository.save(account);
    }

    @Override
    @Transactional
    public Account deposit(AccountDtoOperation dtoOperation) {
        validator.validate(dtoOperation);
        Account account = validator.validate(dtoOperation);
        account.setBalance(account.getBalance().add(dtoOperation.getAmount()));
        return setAccount(dtoOperation, account, DEPOSIT);
    }

    @Override
    @Transactional
    public Account withdraw(AccountDtoOperation dtoOperation) {
        Account account = validator.validate(dtoOperation);
        BigDecimal newBalance = account.getBalance().subtract(dtoOperation.getAmount());
        if (newBalance.compareTo(BigDecimal.ZERO) < 0) {
            throw new AccountOperationException("Недостаточно средств на счете");
        }
        account.setBalance(newBalance);

        return setAccount(dtoOperation, account, WITHDRAW);
    }

    @Override
    @Transactional
    public void transfer(AccountDtoTransfer dtoTransfer) {
        AccountDtoOperation dtoOperation = mapper.map(dtoTransfer, AccountDtoOperation.class);

        Account fromWhom = accountRepository.findByName(dtoTransfer.getName()).orElseThrow(() ->
                new AccountNotFoundException("Счета для снятия с таким именем не существует"));
        Account toWhom = accountRepository.findByName(dtoTransfer.getNameToTransfer()).orElseThrow(() ->
                new AccountNotFoundException("Счета для зачисления с таким именем не существует"));

        fromWhom.setBalance(fromWhom.getBalance().subtract(dtoOperation.getAmount()));
        toWhom.setBalance(toWhom.getBalance().add(dtoOperation.getAmount()));
        Transaction transaction = new Transaction();
        transaction.setAmount(dtoOperation.getAmount());
        transaction.setTransactionType(TRANSFER);
        transaction.setToWhom(toWhom);
        transaction.setFromWhom(fromWhom);
        transactionRepository.save(transaction);
    }

    @Override
    @Transactional
    public List<AccountDtoResponse> getAccounts() {
        return accountRepository.findAll().stream()
                .map(account -> mapper.map(account, AccountDtoResponse.class))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<TransactionDtoResponse> getTransactions(UUID uuid) {
        List<Transaction> allByFromWhomIdOrToWhomId = transactionRepository.findAllByFromWhomIdOrToWhomId(uuid, uuid);
        return allByFromWhomIdOrToWhomId.stream()
                .map(account -> mapper.map(account, TransactionDtoResponse.class))
                .collect(Collectors.toList());
    }

    private Account setAccount(AccountDtoOperation dtoOperation, Account account, TransactionType transactionType) {
        Transaction transaction = new Transaction();
        transaction.setAmount(dtoOperation.getAmount());
        transaction.setTransactionType(transactionType);
        transaction.setToWhom(account);
        transaction.setFromWhom(account);
        transactionRepository.save(transaction);
        return transaction.getFromWhom();
    }
}
